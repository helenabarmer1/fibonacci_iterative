#include <iostream>
using namespace std;

int main()
{
    int first = 0;
    int second = 1;
    int nth_number;

    cout << "Find the nth number of the Fibonacci Sequence. " << endl;
    cout << "Enter the nth number: ";
    cin >> nth_number;

    for(int i=1; i<nth_number; i++)
    {
        second = first + second;
        first = second - first;
    }

    if(nth_number == 1)
    {
        cout << "At position " << nth_number << " you find the number " << 0 << endl;
    }
    else
    {
        cout << "At position " << nth_number << " you find the number " << first << endl;
    }
    return 0;
}